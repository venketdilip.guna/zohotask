# REST API example application


API User Authorization: Basic username: venkat password: Sam@123

GET /login?username=Baskar&amp;password=Sam@123 HTTP/1.1

GET /logout?userid=9 HTTP/1.1

GET /getpost?uid=9 HTTP/1.1

GET /getmypost?uid=1 HTTP/1.1

POST /addpost HTTP/1.1

POST /register HTTP/1.1

GET /addfriend?userid=9&frienduserid=10

GET /removefriend?userid=9&frienduserid=10

GET /deletePost?uid=9&postId=7

POST /sendmessage HTTP/1.1

GET /getmessage?userid=9&toUserId=2

GET /deletemessage?userid=9&messageId=8



## Run the app

    java -cp App.jar ZohoApp 



## Login

### Request

GET /login?username=Baskar&amp;password=Sam@123 HTTP/1.1

Host: localhost:8080

Content-Type: application/json

Authorization: Basic dmVua2F0OlNhbUAxMjM=

Cache-Control: no-cache



    

### Response
HTTP/1.1 200 OK

Server: Zoho Task Java Server v0.0.1 (This Server is buid for social media App Task)

Content-Type: application/json

Connection: close

Content-Size: 34

```JSON
{
    "roleId": 1,
    "userId": 9,
    "fullName": "Baskar",
    "createTime": "Jul 28, 2022 9:17:11 PM",
    "accessTocken": "2Vy-5Uj0qUOFOkfBYTtf1LcXop7Neupp"
}
```
if user is not found 

for wrong   

```JSON
{
    "Error": " user not found"
}

```
