package Util;

public class Local_EN {
    public static final String API_USER_ERROR = "Unauthorized API user";
    public static final String LOGOUT = "User sucessfully LogOut";
    public static final String USER_NOT_LOGIN_ERROR = "User Not Loged In";
    public static final String ERROR_STATUS = "Error";
    public static final String SUCESS_STATUS = "SUCESS";
    public static final String USER_ID_NOT_FOUND="User Id not Found";

    public static final String REGISTRATION_SUCESS = "User Register Sucessfully";
    public static final String USER_EXIST_ERROR="User Already Exists";
    public static final String USER_NOT_FOUND="User Not Found";

    public static final String ADD_FRIEND_SUCESS="Friend Successfully Added";
    public static final String REMOVE_FRIEND_SUCESS="Friend Sucessfully Removed";
    public static final String FRIEND_NOT_FOUND="Friend Id not found";

    public static final String POST_NOT_FOUND="No Post Found";
    public static final String POST_SUCES="post added sucessfully";

    public static final String POST_DELETED="Post Deleted";
    public static final String USER_LOGOUT_SUCESS="User sucessfully logout";

    public static final String MESSAGE_DELETE="Message deleted Sucecss";
    public static final String MESSAGE_ID_NOT_FOUND="Message Id Not Found";
    public static final String MESSAGE_SENT_SUCESS="Message Sucessfully sent";
    
    
    
    private Local_EN() { }
}
