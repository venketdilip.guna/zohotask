import httpserver.HttpServer;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.Map;
import com.google.gson.Gson;
import Controler.AddMyPost;
import Controler.GetMessage;
import Controler.GetMyPost;
import Controler.GetPost;
import Model.Login;
import Util.Local_EN;
import Util.MessageHandler;
import httpserver.HttpRequest;
import httpserver.HttpResponse;
import httpserver.Route;
import services.FriendsService;
import services.LoginService;
import services.MessageService;
import services.PostService;
import services.RegisterService;

public class ZohoApp {

    public static void main(String[] args) {
        Date dateob = new Date();
        String date = dateob.toString();
        try {
            System.setOut(new PrintStream(new File(System.getProperty("user.dir") + date + "_console.log")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpServer server = new HttpServer();
        MessageHandler messagehandler = new MessageHandler();
        server.get(new Route("/getmypost") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    GetMyPost postObject = new GetMyPost();
                    String post = postObject.getPost(request.getParam("uid"));
                    response.setBody(post);
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });
        server.get(new Route("/getpost") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                String userId = request.getParam("uid");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {
                        GetPost postObject = new GetPost();
                        String post = postObject.getPost(userId);
                        if (post.equals("[]")) {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.POST_NOT_FOUND));
                        } else {
                            response.setBody(post);
                        }
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }

            }
        });

        server.post(new Route("/addpost") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                Gson gson = new Gson();
                int row = 0;
                response.setMimeType("application/json");
                Map map = gson.fromJson(request.getHttpBody(), Map.class);
                String id = (String) map.get("userId");
                int userId = Integer.parseInt(id);
                String contentType = (String) map.get("contentType");
                String content = (String) map.get("content");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(userId)) {
                        AddMyPost postObject = new AddMyPost();
                        row = postObject.addPost(userId, contentType, content);
                        response.setBody(messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS, Local_EN.POST_SUCES));
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }

            }
        });

        server.get(new Route("/deletePost") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                PostService service = new PostService();
                String userId = request.getParam("uid");
                String postId = request.getParam("postId");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {
                        service.deletePost(postId);
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS, Local_EN.POST_DELETED));
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }

            }
        });
        server.get(new Route("/login") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    Login userObject = login.loginUser(request.getParam("username"), request.getParam("password"));
                    if (userObject != null) {
                        Gson gson = new Gson();
                        String jsonelement = gson.toJson(userObject);
                        response.setBody(jsonelement);
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_FOUND));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });
        server.get(new Route("/logout") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                int userID = Integer.parseInt(request.getParam("userid"));
                LoginService loginService = new LoginService();
                String tocken = request.getAuthorization();
                if (loginService.checkLogIn(tocken)) {
                    Boolean isLogin = loginService.checkLogIn(userID);
                    if (isLogin) {
                        loginService.LogOut(userID);
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS, Local_EN.USER_LOGOUT_SUCESS));
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });

        server.post(new Route("/register") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                Gson gson = new Gson();
                Map map = gson.fromJson(request.getHttpBody(), Map.class);
                RegisterService registerService = new RegisterService();
                response.setMimeType("application/json");
                String nameOfUser = (String) map.get("nameOfUser");
                String dob = (String) map.get("dob");
                String contactNumber = (String) map.get("contactNumber");
                String email = (String) map.get("email");
                String addressOfUser = (String) map.get("addressOfUser");
                String passwordOfUser = (String) map.get("passwordOfUser");
                LoginService loginService = new LoginService();
                String tocken = request.getAuthorization();
                if (loginService.checkLogIn(tocken)) {
                    if (registerService.isUserExist(nameOfUser, passwordOfUser)) {

                        registerService.registerUser(nameOfUser, dob, contactNumber, email, addressOfUser,
                                passwordOfUser);
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS, Local_EN.REGISTRATION_SUCESS));

                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_EXIST_ERROR));
                    }

                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });
        server.get(new Route("/addfriend") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                String userId = request.getParam("userid");
                String friendUserId = request.getParam("frienduserid");
                LoginService login = new LoginService();
                FriendsService friendObject = new FriendsService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {

                        if (friendObject.checkFriend(friendUserId)) {
                            friendObject.addFriend(friendUserId, userId);
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS,
                                            Local_EN.ADD_FRIEND_SUCESS));
                        } else {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.FRIEND_NOT_FOUND));
                        }
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }

            }
        });
        server.get(new Route("/removefriend") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                String userId = request.getParam("userid");
                String friendUserId = request.getParam("frienduserid");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {
                        FriendsService friendObject = new FriendsService();
                        friendObject.removeFriend(friendUserId, userId);
                        if (friendObject.checkFriend(friendUserId)) {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS,
                                            Local_EN.REMOVE_FRIEND_SUCESS));
                        } else {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS,
                                            Local_EN.FRIEND_NOT_FOUND));
                        }
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS,
                                        Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(
                            messagehandler.setStatusMessage(Local_EN.ERROR_STATUS,
                                    Local_EN.API_USER_ERROR));
                }
            }
        });
        server.post(new Route("/sendmessage") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                Gson gson = new Gson();
                response.setMimeType("application/json");
                Map map = gson.fromJson(request.getHttpBody(), Map.class);
                String fromUserId = (String) map.get("userid");
                String toUserId = (String) map.get("toUserId");
                String content = (String) map.get("content");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(fromUserId))) {
                        MessageService message = new MessageService();
                        if (login.checkUser(toUserId)) {
                            int rowcount = message.sendMessage(fromUserId, toUserId, content);
                            if (rowcount > 0)
                                response.setBody(
                                        messagehandler.setStatusMessage(Local_EN.SUCESS_STATUS,
                                                Local_EN.MESSAGE_SENT_SUCESS));
                        } else {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_ID_NOT_FOUND));
                        }
                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });
        server.get(new Route("/getmessage") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                String userId = request.getParam("userid");
                String toUserId = request.getParam("toUserId");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {
                        GetMessage messageobjec = new GetMessage();
                        if (login.checkUser(toUserId)) {
                            String messages = messageobjec.getMessages(userId, toUserId);
                            response.setBody(messages);
                        } else {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_ID_NOT_FOUND));
                        }

                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });
        server.get(new Route("/deletemessage") {
            @Override
            public void handle(HttpRequest request, HttpResponse response) {
                response.setMimeType("application/json");
                String userId = request.getParam("userid");
                String messageId = request.getParam("messageId");
                LoginService login = new LoginService();
                if (login.checkLogIn(request.getAuthorization())) {
                    if (login.checkLogIn(Integer.parseInt(userId))) {
                        MessageService messaService = new MessageService();
                        if (messaService.deleteMessage(messageId)) {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.MESSAGE_DELETE));

                        } else {
                            response.setBody(
                                    messagehandler.setStatusMessage(Local_EN.ERROR_STATUS,
                                            Local_EN.MESSAGE_ID_NOT_FOUND));
                        }

                    } else {
                        response.setBody(
                                messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.USER_NOT_LOGIN_ERROR));
                    }
                } else {
                    response.setBody(messagehandler.setStatusMessage(Local_EN.ERROR_STATUS, Local_EN.API_USER_ERROR));
                }
            }
        });

        Thread ob = new Thread(server);
        ob.start();
    }
}
