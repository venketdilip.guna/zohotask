-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: localhost    Database: Zohotask
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Media`
--

DROP TABLE IF EXISTS `Media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Media` (
  `media_id` int NOT NULL AUTO_INCREMENT,
  `post_id` int DEFAULT NULL,
  `media_name` varchar(255) DEFAULT NULL,
  `media_size` int DEFAULT NULL,
  `media_type` varchar(255) DEFAULT NULL,
  `media_description` varchar(255) DEFAULT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`media_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Media`
--

LOCK TABLES `Media` WRITE;
/*!40000 ALTER TABLE `Media` DISABLE KEYS */;
/*!40000 ALTER TABLE `Media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages`
--

DROP TABLE IF EXISTS `Messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Messages` (
  `message_id` int NOT NULL AUTO_INCREMENT,
  `message_from_user_id` int DEFAULT NULL,
  `message_to_user_id` int DEFAULT NULL,
  `message_content` varchar(255) DEFAULT NULL,
  `message_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `message_from_user_id` (`message_from_user_id`),
  KEY `message_to_user_id` (`message_to_user_id`),
  CONSTRAINT `Messages_ibfk_1` FOREIGN KEY (`message_from_user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `Messages_ibfk_2` FOREIGN KEY (`message_to_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages`
--

LOCK TABLES `Messages` WRITE;
/*!40000 ALTER TABLE `Messages` DISABLE KEYS */;
INSERT INTO `Messages` VALUES (1,1,2,'test message from user 1','2022-07-26 00:00:00'),(2,2,1,'message from id 2','2022-07-26 00:00:00'),(4,3,1,'message from id 2','2022-07-26 00:00:00'),(5,1,2,'Message from user 1 postman','2022-07-28 20:15:57'),(6,9,2,'Message from user 1 postman','2022-07-28 21:19:40');
/*!40000 ALTER TABLE `Messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Permission`
--

DROP TABLE IF EXISTS `Permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Permission` (
  `permision_id` int NOT NULL AUTO_INCREMENT,
  `permission_role_id` int DEFAULT NULL,
  `permission_name` varchar(255) DEFAULT NULL,
  `permission_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`permision_id`),
  KEY `permission_role_id` (`permission_role_id`),
  CONSTRAINT `Permission_ibfk_1` FOREIGN KEY (`permission_role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Permission`
--

LOCK TABLES `Permission` WRITE;
/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` VALUES (1,1,'public','public');
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friends` (
  `id` int NOT NULL AUTO_INCREMENT,
  `friends_ids` varchar(255) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (1,'2',1),(2,'1',2),(3,'1',3),(4,'3',1),(7,'1',9),(8,'2',9),(9,'3',9),(10,'4',9),(11,'5',9),(12,'6',9),(13,'7',9),(14,'8',9),(16,'10',9);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likes` (
  `like_id` int NOT NULL AUTO_INCREMENT,
  `like_post_id` int DEFAULT NULL,
  `like_user_id` int DEFAULT NULL,
  PRIMARY KEY (`like_id`),
  KEY `like_post_id` (`like_post_id`),
  KEY `like_user_id` (`like_user_id`),
  CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`like_post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`like_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,1,2),(2,2,1),(3,1,3);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login` (
  `login_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `login_user_username` varchar(255) DEFAULT NULL,
  `login_user_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `login_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,1,1,'venkat','Sam@123'),(2,2,1,'jayakumar','12345'),(3,3,1,'Bhavani','dilip@123'),(4,4,1,'Rajeshkumar','raj@123a'),(5,5,1,'guna','guna@123'),(6,6,1,'guna','guna@123'),(7,7,1,'guna','guna@123'),(8,8,1,'guna','guna@123'),(9,9,1,'Baskar','Sam@123'),(10,10,1,NULL,NULL),(11,11,1,NULL,NULL),(12,12,1,NULL,NULL),(13,13,1,'kasthuri','Sam@123'),(14,14,1,'kasthuriy','Sam@123');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_status`
--

DROP TABLE IF EXISTS `login_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login_status` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `create_time` datetime DEFAULT NULL COMMENT 'Create Time',
  `access_tocken` varchar(255) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_status`
--

LOCK TABLES `login_status` WRITE;
/*!40000 ALTER TABLE `login_status` DISABLE KEYS */;
INSERT INTO `login_status` VALUES (21,'2022-07-28 16:04:47','gCc159VGSIvl3_UlyibBwxvJdjlswy7y',5),(29,'2022-07-29 08:37:07','TyNSr-faLP01wf6Wo9XkCxzemRsJUfKo',9);
/*!40000 ALTER TABLE `login_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `post_id` int NOT NULL AUTO_INCREMENT,
  `post_user_id` int DEFAULT NULL,
  `post_date` timestamp NULL DEFAULT NULL,
  `post_type` varchar(255) DEFAULT NULL,
  `post_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `post_user_id` (`post_user_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,'2022-07-24 14:02:08','text','hi his is est post data from id 1'),(2,2,'2022-07-24 14:02:09','text','god morning alll'),(3,3,'2022-07-25 00:00:00','text','test post from bhavani'),(5,4,'2022-07-24 14:02:08','text','hi his is est post data from id 1'),(8,9,'2022-07-28 19:11:47','null','content from post request'),(9,9,'2022-07-28 21:17:40','null','content from post test request'),(10,9,'2022-07-29 08:37:43','null','content from post test request');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'enduser','public users'),(2,'admin','admin access');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shares`
--

DROP TABLE IF EXISTS `shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shares` (
  `share_id` int NOT NULL AUTO_INCREMENT,
  `share_post_id` int DEFAULT NULL,
  `share_user_id` int DEFAULT NULL,
  `share_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`share_id`),
  KEY `share_post_id` (`share_post_id`),
  KEY `share_user_id` (`share_user_id`),
  CONSTRAINT `shares_ibfk_1` FOREIGN KEY (`share_post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `shares_ibfk_2` FOREIGN KEY (`share_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shares`
--

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;
INSERT INTO `shares` VALUES (1,1,2,'test post share'),(2,3,2,'test post share');
/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `user_date_of_birth` date DEFAULT NULL,
  `user_mobile` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'dilip kumar','2022-07-24 13:35:59','1993-07-10','9751325726','venketdilip.guna@gmail.com','no 06,koil street,kanniyam'),(2,'jayakumar ','2022-07-24 13:35:59','1996-01-20','9098787675','jayakumar@gmail.com','no 6 pilliyar koil street kanniyam'),(3,'Bhavani','2022-07-25 00:00:00','1998-09-12','9751325726','bhavani@gmail.com','kanniyam'),(4,'Rajeshkumar','2022-07-24 13:35:59','1998-09-12','98665654','raj.guna@gmail.com','kanniyam'),(5,'guna','2022-07-24 13:35:59','1998-09-12','9976834100','raj.guna@gmail.com','kanniyam'),(6,'guna','2022-07-24 13:35:59','1998-09-12','9976834100','raj.guna@gmail.com','kanniyam'),(7,'guna','2022-07-24 13:35:59','1998-09-12','9976834100','raj.guna@gmail.com','kanniyam'),(8,'guna','2022-07-24 13:35:59','1998-09-12','9976834100','raj.guna@gmail.com','kanniyam'),(9,'Baskar','2022-07-28 19:09:41','2000-01-20','989878766','bas@gmail.com','mambattu'),(10,NULL,'2022-07-28 20:07:25',NULL,NULL,NULL,NULL),(11,NULL,'2022-07-28 20:08:54',NULL,NULL,NULL,NULL),(12,NULL,'2022-07-28 20:11:19',NULL,NULL,NULL,NULL),(13,'kasthuri','2022-07-28 21:18:28','1980-01-20','989878766','kasthuri@gmail.com','kasthuri'),(14,'kasthuriy','2022-07-29 08:38:14','1980-01-20','989878766','kasthuri@gmail.com','kasthuri');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-30  5:44:41
