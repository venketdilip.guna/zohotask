package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import Model.Message;
import Util.DBUtil;

public class MessageService {

    Connection con;

    public int sendMessage(String fromUserId, String toUserId, String content) {
        return sendMessage(Integer.parseInt(fromUserId), Integer.parseInt(toUserId), content);
    }

    public boolean deleteMessage(String messageId) {
        return deleteMessage(Integer.parseInt(messageId));
    }

    public int sendMessage(int fromUserId, int toUserId, String content) {
        int rowcount = 0;
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        Date date = ts;
        String dateOfMessage = date.toString();
        try {
            String sql = "INSERT INTO"
                    + " `Messages`(message_from_user_id,message_to_user_id,message_content,message_time)"
                    + " VALUES(" + fromUserId + "," + toUserId + ",'" + content + "','" + dateOfMessage + "')";
            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            rowcount = stmt.executeUpdate(sql);
            if (rowcount > 0) {
                return rowcount;

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return rowcount;
    }

    public List<Message> getMessage(String userId, String toUserId) {
        List<Message> messages = new ArrayList<Message>();
        int fid = Integer.parseInt(userId);
        int tid = Integer.parseInt(toUserId);
        String sql = "SELECT * FROM `Messages` WHERE (message_from_user_id=" + fid + " and message_to_user_id=" + tid
                + ") or (message_from_user_id=" + tid + " and message_to_user_id=" + fid
                + ")  ORDER BY message_time DESC ";
        try {
            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Message message = new Message();
                message.setMessageToUserId(rs.getInt("message_to_user_id"));
                message.setMessageFromUserId(rs.getInt("message_from_user_id"));
                message.setMessageTime(rs.getTimestamp("message_time"));
                message.setMessageContent(rs.getString("message_content"));
                message.setMessageId(rs.getInt("message_id"));
                messages.add(message);
            }
            return messages;
        } catch (Exception e) {

        }
        return null;
    }

    public boolean deleteMessage(int messageId) {
        String sql = "DELETE FROM Messages WHERE message_id =" + messageId;
        int rowcount = 0;
        try {
            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            rowcount = stmt.executeUpdate(sql);
            if (rowcount > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;

    }
}
