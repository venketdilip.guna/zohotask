package services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import Model.Post;
import Util.DBUtil;

public class PostService {
    Post post;
    Connection con = null;

    public int savePost(int postUserId,String contentType,String content) {
        int rc = 0;
        Timestamp ts=new Timestamp(System.currentTimeMillis());  
        Date date=ts;  
        String postdate=date.toString();                    
        try {

            String sql = "INSERT INTO  "
                    + "`posts` (`post_user_id`, `post_date`, `post_type`, `post_content`)"
                    + " VALUES ("+postUserId+", '"+postdate+"', '"+contentType+"', '"+content+"')";

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            rc = stmt.executeUpdate(sql);
            if (rc > 0) {
                System.out.println(rc);

            }
        } catch (Exception e) {

            System.out.println(e);
        }
        return rc;

    }

    public List<Post> getPostsOfUser(Integer userId) {

        List<Post> posts = new ArrayList<Post>();

        try {

            String sql = "SELECT DISTINCT *,"
                    + " (SELECT COUNT(*) FROM likes"
                    + "  WHERE like_post_id = posts.post_id) AS likes,"
                    + "  (SELECT users.full_name FROM users"
                    + "  WHERE user_id = posts.post_user_id) AS username,"
                    + "  (SELECT media_url FROM Media"
                    + "  WHERE post_id = posts.post_id) As media_url,"
                    + "  (SELECT COUNT(*) FROM shares"
                    + "  WHERE share_post_id = posts.post_id) AS shares"
                    + " FROM posts INNER JOIN (select friends_ids from friends where user_id=" + userId
                    + ") as friend ON posts.post_user_id = friend.friends_ids ORDER BY post_date DESC ;";

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                post = new Post();
                post.setPostId(rs.getInt("post_id"));
                post.setPostUserId(rs.getInt("post_user_id"));
                post.setPostDate(rs.getTimestamp("post_date"));
                post.setPostType(rs.getString("post_type"));
                post.setPostContent(rs.getString("post_content"));
                post.setFriendsId(rs.getInt("friends_ids"));
                post.setLikes(rs.getInt("likes"));
                post.setUsername(rs.getString("username"));
                post.setMediaUrl(rs.getString("media_url"));
                post.setShares(rs.getInt("shares"));
                posts.add(post);

            }

        } catch (Exception e) {

            System.out.println(e);
        }

        return posts;

    }

    public List<Post> getAllPost(Integer userId) {
        List<Post> posts = new ArrayList<Post>();

        try {

            String sql = "SELECT DISTINCT *,"
                    + " (SELECT COUNT(*) FROM likes"
                    + "  WHERE like_post_id = posts.post_id) AS likes,"
                    + "  (SELECT users.full_name FROM users"
                    + "  WHERE user_id = posts.post_user_id) AS username,"
                    + "  (SELECT media_url FROM Media"
                    + "  WHERE post_id = posts.post_id) As media_url,"
                    + "  (SELECT COUNT(*) FROM shares"
                    + "  WHERE share_post_id = posts.post_id) AS shares"
                    + " FROM posts where post_user_id = " + userId + " ORDER BY post_date DESC";

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                post = new Post();
                post.setPostId(rs.getInt("post_id"));
                post.setPostUserId(rs.getInt("post_user_id"));
                post.setPostDate(rs.getTimestamp("post_date"));
                post.setPostType(rs.getString("post_type"));
                post.setPostContent(rs.getString("post_content"));
                post.setLikes(rs.getInt("likes"));
                post.setUsername(rs.getString("username"));
                post.setMediaUrl(rs.getString("media_url"));
                post.setShares(rs.getInt("shares"));
                posts.add(post);

            }

        } catch (Exception e) {

            System.out.println(e);
        }

        return posts;

    }
    public Boolean deletePost(String postId) {
        return deletePost(Integer.parseInt(postId));
    }
    public Boolean deletePost(Integer postId) {

        String sql = "{CALL delete_post(?)}";

        try {
            con = DBUtil.getConnection();
            CallableStatement cstmt = con.prepareCall(sql);
            cstmt.setInt(1, postId);
            int rows = cstmt.executeUpdate();
            System.out.println("deleted posts : " + rows);
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return true;

    }
    public int postLike(int postId,int userId)
    {
       int rowcount=0;
        try {

            String sql =  "INSERT INTO likes(like_post_id,like_user_id) VALUES("+"postId"+","+userId+")";
            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            rowcount = stmt.executeUpdate(sql);
            if (rowcount> 0) {
                return rowcount;

            }
        } catch (Exception e) {

            System.out.println(e);
        }
        return rowcount;
    }

}
