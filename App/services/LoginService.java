package services;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import Model.Login;
import Util.DBUtil;

public class LoginService {
    Connection con = null;
    private static final SecureRandom secureRandom = new SecureRandom();
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    public static String generateNewToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
    public boolean checkUser(String UserId) {
        return checkUser(Integer.parseInt(UserId)) ;
    }
    public Login loginUser(String userName, String passWord) {
        Login login = null;
        try {
            String sql = "SELECT login.role_id, users.user_id, users.full_name"
                    + " FROM login "
                    + " RIGHT JOIN users ON login.user_id = users.user_id  "
                    + " WHERE `login_user_username`='" + userName + "' and `login_user_password`='" + passWord + "'";
            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                if( ! checkLogIn(rs.getInt("user_id"))){
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    Date createdat = timestamp;
                    String tocken = generateNewToken();
                    Statement sstmt = con.createStatement();
                    String statusSql = "INSERT INTO "
                            + "login_status(create_time,access_tocken,user_id) "
                            + "VALUES('" + createdat.toString() + "','" + tocken + "'," + rs.getInt("user_id") + ")";
                    int rows = sstmt.executeUpdate(statusSql);
                    if (rows > 0) {
                        login = new Login();
                        login.setAccessTocken(tocken);
                        login.setCreateTime(timestamp);
                        login.setFullName(rs.getString("full_name"));
                        login.setRoleId(rs.getInt("role_id"));
                        login.setUserId(rs.getInt("user_id"));
                }
              
                    return login;
                }
                else{
                        login = new Login();
                        login.setFullName(rs.getString("full_name"));
                        login.setRoleId(rs.getInt("role_id"));
                        login.setUserId(rs.getInt("user_id"));
                        return login;

                }

            }
        } catch (Exception e) {

            System.out.println(e);
        }
        return null;
    }

    public boolean checkLogIn(int userID) {
        String sql = "SELECT create_time,access_tocken FROM login_status WHERE user_id=" + userID;
        try {

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkLogIn(int userID, String tocken) {
        String sql = "SELECT create_time,access_tocken FROM login_status WHERE user_id=" + userID
                + " and access_tocken='" + tocken + "'";
        try {

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkLogIn(String tocken) {
        if(tocken!=null){
        byte[] decodedBytes = Base64.getDecoder().decode(tocken.split(" ")[1]);
        String decodedString[] = new String(decodedBytes).split(":");
       
        String userName=decodedString[0];
        String passWord=decodedString[1];
        System.out.println(userName +" "+passWord);
        String sql = "SELECT * FROM login WHERE login_user_username='" + userName
                + "' and login_user_password='" + passWord + "'";
        try {

            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next() && rs.getInt("role_id")==2 ) {
              
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
        return false;
    }
   
   
    public void LogOut(int userID) {
        String sql = "DELETE FROM login_status WHERE user_id="+userID;
        try {

            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public boolean checkUser(int UserId) {
        String sql = "SELECT * FROM users WHERE user_id =" + UserId ;
        ResultSet rowcount=null;
        try {

            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            rowcount= stmt.executeQuery(sql);
            if (rowcount.next()) {
                return true;

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;            
    }

}
