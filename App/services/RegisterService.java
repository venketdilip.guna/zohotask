package services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import Util.DBUtil;

public class RegisterService {
    Connection con = null;

    public Boolean registerUser(String nameOfUser, String dob, String contactNumber, String email, String addressOfUser,
            String passwordOfUser) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        Date date = ts;
        String createDate = date.toString();
        String sql = "{CALL create_user(?,?,?,?,?,?,?)}";

        try {
            con = DBUtil.getConnection();
            CallableStatement cstmt = con.prepareCall(sql);
            cstmt.setString(1, nameOfUser);
            cstmt.setString(2, createDate);
            cstmt.setString(3, dob);
            cstmt.setString(4, contactNumber);
            cstmt.setString(5, email);
            cstmt.setString(6, addressOfUser);
            cstmt.setString(7, passwordOfUser);
            int rows = cstmt.executeUpdate();
            System.out.println("user registed: " + rows);
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return true;

    }

    public boolean isUserExist(String userName,String passWord) {
        
       
        String sql = "SELECT * FROM login  WHERE `login_user_username`='" + userName + "' and `login_user_password`='" + passWord + "'";
        try{
        con = DBUtil.getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        if (rs.next()) {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
            return true;
    }
}
