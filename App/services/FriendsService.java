package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import Util.DBUtil;

public class FriendsService {
    Connection con;
    public int addFriend(String friendUserId, String userId) {
        return addFriend(Integer.parseInt(friendUserId) ,Integer.parseInt(userId));
    }
    public boolean removeFriend(String friendUserId, String userId) {
        return removeFriend(Integer.parseInt(friendUserId) ,Integer.parseInt(userId));
    }
    public boolean checkFriend(String friendUserId) {
        return checkFriend(Integer.parseInt(friendUserId));
    }
    public int addFriend(int friendUserId, int userId) {
        int rowcount = 0;
        try {
            String sql = "INSERT INTO friends(friends_ids,user_id) VALUES('" + friendUserId + "'," + userId + ")";
            con = DBUtil.getConnection();
            java.sql.Statement stmt = con.createStatement();
            rowcount = stmt.executeUpdate(sql);
            if (rowcount > 0) {
                return rowcount;
            }
        } catch (Exception e) {

            System.out.println(e);
        }
        return rowcount;
    }

    public boolean removeFriend(int friendUserId, int userId) {
        String sql = "DELETE FROM friends WHERE friends_ids =" + friendUserId + " and user_id=" + userId;
        int rowcount=0;
        try {
            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            rowcount= stmt.executeUpdate(sql);
            if (rowcount > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;            
    }
    public boolean checkFriend(int friendUserId) {
        String sql = "SELECT FROM friends WHERE friends_ids =" + friendUserId ;
        ResultSet rowcount=null;
        try {

            con = DBUtil.getConnection();
            Statement stmt = con.createStatement();
            rowcount= stmt.executeQuery(sql);
            if (rowcount.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;            
    }
    

}
