package Controler;

import java.util.List;

import com.google.gson.Gson;

import Model.Message;
import services.MessageService;

public class GetMessage {
    public String getMessages(String userId, String toUserId) {
        Gson gson = new Gson();
        MessageService message = new MessageService();
        List<Message> messages = message.getMessage(userId, toUserId);
        String allmessages = gson.toJson(messages);
        return allmessages;
    }
}
