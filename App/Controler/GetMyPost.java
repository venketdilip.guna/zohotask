package Controler;
import java.util.List;
import com.google.gson.Gson;
import Model.Post;
import services.PostService;

public class GetMyPost {

    public String getPost(String userId) {
        Gson gson = new Gson();
        PostService service = new PostService();
        List<Post> allmypost = service.getAllPost(Integer.parseInt(userId));
        String postJson = gson.toJson(allmypost);
        return postJson;
    }

}
