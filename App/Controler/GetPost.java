package Controler;
import java.util.List;
import com.google.gson.Gson;

import Model.Post;
import services.PostService;

public class GetPost {

    public String getPost(String userId) {

        PostService service = new PostService();
        List<Post> friendspost = service.getPostsOfUser(Integer.parseInt(userId));
        Gson gson = new Gson();
        String jsonelement = gson.toJson(friendspost);
        return jsonelement;

    }

}
