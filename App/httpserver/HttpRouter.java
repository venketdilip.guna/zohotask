package httpserver;

import java.util.HashMap;
import java.util.Map;

public class HttpRouter {
    private Map<String, HttpHandler> handlers;
    private HttpHandler defaultHandler;

    public HttpRouter() {
        handlers = new HashMap<>();
        defaultHandler = null;
    };

    public HttpHandler route(String pathSegment, HttpRequest request) {

        if (getHandlers().containsKey(pathSegment)) {
            request.setPath(request.getPath().substring(pathSegment.length() + 1));
            return getHandlers().get(pathSegment);
        } else if (defaultHandler != null) {
            return defaultHandler;
        }

        return defaultHandler;
    }

    public Map<String, HttpHandler> getHandlers() {
        return handlers;
    }

    public void addHandler(String pathSegment, HttpHandler handler) {
        getHandlers().put(pathSegment, handler);
    }

    public void setDefaultHandler(HttpHandler handler) {
        defaultHandler = handler;
    }

    public HttpHandler getDefaultHandler() {
        return defaultHandler;
    }
}
