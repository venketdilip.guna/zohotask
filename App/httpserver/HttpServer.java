package httpserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class HttpServer extends HttpHandler implements Runnable {

    public static final int defaultPort = 8080;

    private static String serverName = "Zoho Task Java Server";

    private static String serverVersion = "0.0.1";

    private static String serverETC = "This Server is buid for social media App Task";

    public int port;
    private ServerSocket socket = null;
    private HttpRouter router;

    private boolean running = true;
    FileHandler fh;  
    private Logger logger = Logger.getLogger("java-httpserver");

    public HttpServer() {
        this(defaultPort);
    }

    public HttpServer(int port) {
        setPort(port);
        setRouter(new HttpRouter());
        getRouter().setDefaultHandler(this);
    }

    public void run() {
        try {
            running = true;
            fh = new FileHandler("MyLogFile.log");  
            logger.addHandler(fh);
            socket = new ServerSocket();
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter); 
            logger.info("Starting HttpServer at http://127.0.0.1:" + getPort());

            socket.setReuseAddress(true);
            socket.bind(new InetSocketAddress(getPort()));

            while (running) {
                Socket connection = null;
                try {
                    connection = socket.accept();
                    HttpRequest request = new HttpRequest(getRouter(), connection);
                    Thread t = new Thread(request);
                    t.start();
                    logger.info(String.format("Http request from %s:%d", connection.getInetAddress(),
                            connection.getPort()));

                } catch (SocketException e) {

                    logger.log(Level.WARNING, "Client broke connection early!", e);

                } catch (IOException e) {

                    logger.log(Level.WARNING, "IOException. Probably an HttpRequest issue.", e);

                } catch (HttpException e) {

                    logger.log(Level.WARNING, "HttpException.", e);

                } catch (Exception e) {

                    logger.log(Level.SEVERE, "Generic Exception!", e);
                    break;
                }
            }
        } catch (Exception e) {

            logger.log(Level.WARNING, "Something bad happened...", e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                logger.log(Level.WARNING, "Well that's not good...", e);
            }
        }

        logger.info("Server shutting down.");
    }

    public void setRouter(HttpRouter router) {
        this.router = router;
    }

    public HttpRouter getRouter() {
        return this.router;
    }

    public static void setServerInfo(String name, String version, String etc) {
        serverName = name;
        serverVersion = version;
        serverETC = etc;
    }

    public static String getServerName() {
        return serverName;
    }

    public static String getServerVersion() {
        return serverVersion;
    }

    public static String getServerETC() {
        return serverETC;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public void stop() {
        running = false;

        try {
            socket.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Error closing socket.", e);
        }
    }
}
