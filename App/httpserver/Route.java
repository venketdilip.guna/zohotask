package httpserver;

import java.util.List;
import java.util.ArrayList;


public abstract class Route {
    private List<String> routePath = new ArrayList<>();
    public Route(String path) {
        String[] pathSegments = cleanPath(path).split("/");
        for (int i = 0; i < pathSegments.length; i++) {
            if (pathSegments[i].isEmpty()) {
                continue;
            }
            routePath.add(pathSegments[i]);
            
        }
    }

    public void invoke(HttpRequest request, HttpResponse response) {
        try {
            List<String> calledPath = request.getSplitPath();
            System.out.println(calledPath);
            handle(request, response);
        } catch (Throwable t) {
            response.error(500, t.getMessage(), t);
        }
    }

 

    public boolean matchesPerfectly(List<String> path) {
        return routePath.equals(path);
    }


    public static String cleanPath(String path) {
        path = path.trim();
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }

        return path;
    }

 
    public abstract void handle(HttpRequest request, HttpResponse response);
}
