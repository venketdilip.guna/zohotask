package Model;

import java.sql.Timestamp;

public class Message {
    private Integer messageId;
    private Integer messageFromUserId;
    private Integer messageToUserId;
    private String messageContent;
    private Timestamp messageTime;
    public Integer getMessageId() {
        return messageId;
    }
    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }
    public Integer getMessageFromUserId() {
        return messageFromUserId;
    }
    public void setMessageFromUserId(Integer messageFromUserId) {
        this.messageFromUserId = messageFromUserId;
    }
    public Integer getMessageToUserId() {
        return messageToUserId;
    }
    public void setMessageToUserId(Integer messageToUserId) {
        this.messageToUserId = messageToUserId;
    }
    public String getMessageContent() {
        return messageContent;
    }
    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
    public Timestamp getMessageTime() {
        return messageTime;
    }
    public void setMessageTime(Timestamp messageTime) {
        this.messageTime = messageTime;
    }
}
