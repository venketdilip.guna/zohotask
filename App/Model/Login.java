package Model;

import java.sql.Timestamp;

public class Login {

    private Integer roleId;
    private Integer userId;
    private String fullName;
    private Timestamp createTime;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getAccessTocken() {
        return accessTocken;
    }

    public void setAccessTocken(String accessTocken) {
        this.accessTocken = accessTocken;
    }

    private String accessTocken;

}
