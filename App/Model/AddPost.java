package Model;

import java.sql.Date;

public class AddPost {
    private Integer postUserId;
    private Date postDate;
    private String postType;
    private String postContent;
    public Integer getPostUserId() {
        return postUserId;
    }
    public void setPostUserId(Integer postUserId) {
        this.postUserId = postUserId;
    }
    public Date getPostDate() {
        return postDate;
    }
    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }
    public String getPostType() {
        return postType;
    }
    public void setPostType(String postType) {
        this.postType = postType;
    }
    public String getPostContent() {
        return postContent;
    }
    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }
}
