-- Active: 1658804547716@@52.53.151.67@3306@Zohotask
CREATE DEFINER=`vScopeUserName`@`%` PROCEDURE `create_user`(IN nameofuser varchar(255),IN createdat TIMESTAMP,IN dob varchar(255),IN contactnumber varchar(255) ,IN email varchar(255),IN addressofuser varchar(255) , IN passwordofuser varchar(255))
BEGIN
INSERT INTO `users` 
( `full_name`, `created_at`, `user_date_of_birth`, `user_mobile`, `user_email`, `user_address`) 
VALUES 
(nameofuser, createdat, dob,contactnumber, email, addressofuser);

INSERT INTO login 
(user_id,role_id,login_user_username,login_user_password) 
VALUES(LAST_INSERT_ID(),1,nameofuser, passwordofuser );

COMMIT;
END


-- Active: 1658804547716@@52.53.151.67@3306@Zohotask
CREATE DEFINER=`vScopeUserName`@`%` PROCEDURE `delete_post`(IN postid int)
BEGIN

DELETE FROM likes WHERE like_post_id = postid;
DELETE FROM posts WHERE post_id = postid;
DELETE FROM shares WHERE share_post_id = postid;
DELETE FROM Media WHERE post_id = postid;

COMMIT;
END


-- Active: 1658804547716@@52.53.151.67@3306@Zohotask
CREATE DEFINER=`vScopeUserName`@`%` PROCEDURE `update_user`(IN userid int, IN roleid int, IN nameofuser varchar(255),IN createdat TIMESTAMP,IN dob varchar(255),IN contactnumber varchar(255) ,IN email varchar(255),IN addressofuser varchar(255) , IN passwordofuser varchar(255))
BEGIN
UPDATE `users` 
SET `full_name`=nameofuser, `created_at`=createdat, `user_date_of_birth`=dob, `user_mobile`=contactnumber, `user_email`=email, `user_address`=addressofuser 
WHERE `user_id`=userid;


UPDATE `login`
SET `role_id`=roleid,`login_user_username`=nameofuser,`login_user_password`=passwordofuser
WHERE `user_id`=userid;

COMMIT;
END